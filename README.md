# recher_emplois_BD

## Présentation

Ce repo est actuellement utiliser pour tester la construction d'une image docker depuis un Dockerfile et directement dans le CI.

Pour le test , nous avons utilisé l'image du repos "recher_emplois_Fastapi".

### Préréquis

- avoir à la racine de notre projet les fichiers suivants:
    * run.sh
    * poetry.lock
    * pyproject.toml
    * le Dockerfile à builder
    *  et enfin , le fichier .gitlab-ci.yml   

### Documentation utilisées
* https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
* https://www.mikestreety.co.uk/blog/build-and-push-a-docker-image-to-a-private-registry-with-gitlab-ci/
* https://docs.docker.com/engine/reference/commandline/login/#credentials-store


## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
About future releases, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

## Authors and acknowledgment
Thanks Devops_Interview team !

## License
Open source projects.

## Project status
In progress